# Cmake

Pipeline to build recent version of Cmake for our legacy CentOS 6 32 and 64 bits platform.

Allows to build Cmake for:
* CentOS 6.10 64 bits: [cmake-3.31.5-linux-x86_64.zip](https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/cmake/-/jobs/artifacts/3.31.5/download?job=linux-x86_64)
* CentOS 6.10 32 bits: [cmake-3.31.5-linux-i686.zip](https://gitlab.synchrotron-soleil.fr/software-control-system/devtools/cmake/-/jobs/artifacts/3.31.5/download?job=linux-i686)
